﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Apple : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] List<Sprite> appleSprites;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Transform myTransform;

    Sheep player;
    Sounds sounds;
    float fallingSpeed = 6f;
    bool interactable = false;
    bool isfalled;

    void OnEnable()
    {
        if (sounds == null)
        {
            sounds = FindObjectOfType<Sounds>();
        }
        isfalled = false;
        interactable = false;

        if (player == null)
        {
            player = FindObjectOfType<Sheep>();
        }
        spriteRenderer.sprite = appleSprites[Random.Range(0, appleSprites.Count)];
    }

    void Update()
    {
        if (myTransform.position.y > -4.05f)
        {
            myTransform.position -= new Vector3(0, fallingSpeed * Time.deltaTime, 0);
        }
        else
        {
            if (!isfalled)
            {
                sounds.appleDrop.Play();
                isfalled = true;
                interactable = true;
            }
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (interactable == true)
        {
            player.Move(myTransform.position);
            interactable = false;
        }
    }
}
