﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusDesk : MonoBehaviour
{
    [SerializeField] List<GameObject> parts;
    [SerializeField] Button bttn;
    int openParts = 0;
    int speed = 50;
    Transform myTransform;
    Sounds sounds;

    private void OnEnable()
    {
        if (sounds == null)
        {
            sounds = FindObjectOfType<Sounds>();
        }
        bttn.enabled = false;
        if (myTransform == null)
        {
            myTransform = GetComponent<Transform>();
        }

        if (!PlayerPrefs.HasKey("BonusPuzzleParts"))
        {
            PlayerPrefs.SetInt("BonusPuzzleParts", 0);
            PlayerPrefs.Save();
        }
        openParts = PlayerPrefs.GetInt("BonusPuzzleParts");

        for (int i = 0; i < openParts; i++)
        {
            if (!parts[i].activeSelf)
            {
                parts[i].SetActive(true);
            }
        }
        StartCoroutine(ShowDeck());
    }
    bool CheckParts()
    {
        if (!PlayerPrefs.HasKey("CatchedApplesNum")
            || openParts >= parts.Count
            || openParts > PlayerPrefs.GetInt("CatchedApplesNum") / 10)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void ShowNewPart()
    {
        openParts = PlayerPrefs.GetInt("CatchedApplesNum") / 10;
        PlayerPrefs.SetInt("BonusPuzzleParts", openParts);
        PlayerPrefs.Save();
    }

    IEnumerator ShowDeck()
    {
        sounds.turn.Play();
        while (myTransform.position.x > 0.1f)
        {
            myTransform.position = new Vector3(myTransform.position.x - Time.deltaTime * speed, 0, 0);
            yield return null;
        }
        myTransform.position = new Vector3(0, 0, 0);

        if (CheckParts())
        {
            ShowNewPart();
        }
        bttn.enabled = true;
    }

    public IEnumerator HideDeck()
    {
        bttn.enabled = false;
        sounds.turn.Play();
        while (myTransform.position.x < 24.9)
        {
            myTransform.position = new Vector3(myTransform.position.x + Time.deltaTime * speed, 0, 0);
            yield return null;
        }
        myTransform.position = new Vector3(25, 0, 0);
        bttn.enabled = true;
        gameObject.SetActive(false);
    }
}
