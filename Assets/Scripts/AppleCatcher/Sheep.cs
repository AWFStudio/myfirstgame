﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Sheep : MonoBehaviour
{
    float speed = 2f;

    [SerializeField] Animator anim;
    [SerializeField] GameObject bonusDesk;
    [SerializeField] Transform myTransform;
    [SerializeField] GameObject apple;
    [SerializeField] float stepInterval;

    AppleCounter counter;
    Sounds sounds;
    private bool isMoving;
    float countdown = 0;

    private void Start()
    {
        sounds = FindObjectOfType<Sounds>();
        counter = FindObjectOfType<AppleCounter>();
    }

    private void Update()
    {
        if (isMoving)
        {
            if (countdown >= stepInterval)
            {
                sounds.footstep.pitch = Random.Range(0.9f, 1.1f);
                sounds.footstep.Play();
                countdown = 0;
            }
            else
            {
                countdown += Time.deltaTime;
            }
        }
    }

    internal void Move(Vector3 applePos)
    {
        if (myTransform.position.x > applePos.x)
        {
            StartCoroutine(WalkLeft(applePos));
        }
        else if (myTransform.position.x < applePos.x)
        {
            StartCoroutine(WalkRight(applePos));
        }
    }

    IEnumerator WalkLeft(Vector3 applePos)
    {
        anim.SetBool("WalkingLeft", true);
        isMoving = true;
        while (myTransform.position.x > applePos.x)
        {
            myTransform.position = new Vector3(myTransform.position.x - Time.deltaTime * speed, myTransform.position.y, 0);
            yield return null;
        }
        isMoving = false;
        anim.SetBool("WalkingLeft", false);
        StartCoroutine(EatApple());
    }

    IEnumerator WalkRight(Vector3 applePos)
    {
        anim.SetBool("WalkingRight", true);
        isMoving = true;
        while (myTransform.position.x < applePos.x)
        {
            myTransform.position = new Vector3(myTransform.position.x + Time.deltaTime * speed, myTransform.position.y, 0);
            yield return null;
        }
        isMoving = false;
        anim.SetBool("WalkingRight", false);
        StartCoroutine(EatApple());
    }

    IEnumerator EatApple()
    {
        anim.SetBool("isChewing", true);
        sounds.chewing.Play();
        while (sounds.chewing.isPlaying)
        {
            yield return null;
        }
        anim.SetBool("isChewing", false);
        apple.SetActive(false);
        counter.UpdateCounter();
    }
}
