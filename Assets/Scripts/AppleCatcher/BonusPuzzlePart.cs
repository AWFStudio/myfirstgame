﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPuzzlePart : MonoBehaviour
{
    [SerializeField] Animation myAnimation;

    private void OnEnable()
    {
        myAnimation.Play("BonusPuzzlePart");
    }
}
