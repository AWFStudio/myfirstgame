﻿using UnityEngine;
using System.Collections.Generic;
public class AppleSpawner : MonoBehaviour
{
    [SerializeField] Transform apple;

    void Start()
    {
        SpawnApple();
    }

    public void SpawnApple()
    {
        if (!apple.gameObject.activeSelf)
        {
            apple.position = new Vector2(Random.Range(-7f, 7f), 6f);
            apple.gameObject.SetActive(true);
        }
    }
}
