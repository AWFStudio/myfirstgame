﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AppleCounter : MonoBehaviour
{
    [SerializeField] int score;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] GameObject bonusDesk;

    AppleSpawner spawner;

    void Start()
    {
        spawner = FindObjectOfType<AppleSpawner>();
        if (!PlayerPrefs.HasKey("CatchedApplesNum"))
        {
            PlayerPrefs.SetInt("CatchedApplesNum", 0);
            PlayerPrefs.Save();
        }
        score = PlayerPrefs.GetInt("CatchedApplesNum");
        scoreText.text = score.ToString();
    }

    public void UpdateCounter()
    {
        score++;
        PlayerPrefs.SetInt("CatchedApplesNum", score);
        PlayerPrefs.Save();
        scoreText.text = score.ToString();
        if (score % 10 == 0)
        {
            bonusDesk.SetActive(true);
        }
        else
        {
            spawner.SpawnApple();
        }
    }
}
