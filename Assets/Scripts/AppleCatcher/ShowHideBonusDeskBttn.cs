﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideBonusDeskBttn : MonoBehaviour
{
    [SerializeField] GameObject bonusDeskGO;
    BonusDesk bonusDesk;
    private void Start()
    {
        bonusDesk = bonusDeskGO.GetComponent<BonusDesk>();
    }

    public void Press()
    {
        if (!bonusDeskGO.activeSelf)
        {
            bonusDeskGO.SetActive(true);
        }
        else
        {
            StartCoroutine(bonusDesk.HideDeck());
        }
    }
}
