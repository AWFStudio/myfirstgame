﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PartMover : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{

    [SerializeField] int zoneNum;
    [SerializeField] GameObject pos;
    PuzzleSceneManager scene;
    Sounds sounds;
    Canvas canvas;
    RectTransform rectTransform;
    Vector3 startingPosition;
    Vector3 targetPosition;
    bool returnToStartingPosition = false;
    bool isCatched;
    float speed = 600f;

    public int ZoneNum { get => zoneNum; }
    public bool IsCatched { get => isCatched; set => isCatched = value; }
    void Awake()
    {
        scene = FindObjectOfType<PuzzleSceneManager>();
        rectTransform = GetComponent<RectTransform>();
        canvas = FindObjectOfType<Canvas>();
        sounds = FindObjectOfType<Sounds>();
    }
    void Update()
    {
        if (returnToStartingPosition == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, startingPosition, speed * Time.deltaTime);
        }
        if (isCatched == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startingPosition = rectTransform.position;
        targetPosition = pos.GetComponent<RectTransform>().position;

        returnToStartingPosition = false;
        transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isCatched == false)
        {
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (IsCatched == false)
        {
            returnToStartingPosition = true;
            sounds.wrong.Play();
        }
    }

}
