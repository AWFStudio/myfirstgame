﻿using System.Collections;
using UnityEngine;

public class PartCatcher : MonoBehaviour
{
    [SerializeField] int zoneNum;
    PuzzleController image;
    PuzzleSceneManager scene;
    Sounds sounds;

    private void Start()
    {
        scene = FindObjectOfType<PuzzleSceneManager>();
        image = FindObjectOfType<PuzzleController>();
        sounds = FindObjectOfType<Sounds>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PartMover>() != null)
        {
            if (other.GetComponent<PartMover>().ZoneNum == zoneNum)
            {
                other.GetComponent<PartMover>().IsCatched = true;
                sounds.correct.Play();
                StartCoroutine(AddCount());
            }
        }
    }

    IEnumerator AddCount()
    {
        yield return new WaitForSeconds(0.5f);
        image.DetailsCounter++;
        image.CheckCount();
    }
}
