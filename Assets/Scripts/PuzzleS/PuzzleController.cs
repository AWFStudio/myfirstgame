﻿using System.Collections;
using UnityEngine;

public class PuzzleController : MonoBehaviour
{
    int detailsCounter = 0;
    [SerializeField] GameObject fullImage;
    [SerializeField] GameObject partsPanel;
    PuzzleSceneManager scene;
    Sounds sounds;

    public int DetailsCounter { get => detailsCounter; set => detailsCounter = value; }

    private void Start()
    {
        scene = FindObjectOfType<PuzzleSceneManager>();
        sounds = FindObjectOfType<Sounds>();
        StartCoroutine(StartTheLevel());
    }

    public void CheckCount()
    {
        if (DetailsCounter == 4)
        {
            StartCoroutine(Win());
        }
    }

    IEnumerator Win()
    {
        fullImage.SetActive(true);
        fullImage.gameObject.GetComponent<Animation>().Play("Increase Image");
        sounds.littleWin.Play();
        yield return new WaitForSeconds(1.5f);
        scene.nextBttn.gameObject.SetActive(true);
    }

    IEnumerator StartTheLevel()
    {
        yield return new WaitForSeconds(0.1f);
        partsPanel.SetActive(true);
    }


}
