﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzleSceneManager : MonoBehaviour
{

    [SerializeField] Transform canvasTransform;
    public GameObject nextBttn;
    PuzzleController currentLevel;
    int levelNum;

    void Start()
    {
        if (!PlayerPrefs.HasKey("PuzzlesLevelNum"))
        {
            PlayerPrefs.SetInt("PuzzlesLevelNum", 1);
            PlayerPrefs.Save();
        }
        levelNum = PlayerPrefs.GetInt("PuzzlesLevelNum");
        ShowNextLevel();
    }

    public void ContinueBttn()
    {
        if (levelNum < 10) { levelNum++; }
        else { levelNum = 1; }

        Destroy(currentLevel.gameObject);
        PlayerPrefs.SetInt("PuzzlesLevelNum", levelNum);
        PlayerPrefs.Save();
        nextBttn.SetActive(false);
        ShowNextLevel();
    }
    void ShowNextLevel()
    {
        GameObject newLevel = Instantiate(Resources.Load<GameObject>("Prefabs/Puzzles/Puzzle " + levelNum));
        newLevel.transform.SetParent(canvasTransform, false);
        newLevel.transform.SetAsFirstSibling();
        currentLevel = FindObjectOfType<PuzzleController>();
    }




}
