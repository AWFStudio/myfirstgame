﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinnerImg : MonoBehaviour
{
    [SerializeField] Animation anim;
    private void OnEnable()
    {
        anim.Play("Increase Image");
    }

}
