﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OptionsBttn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] GameObject optionsPanel;
    [SerializeField] GameObject circle;
    [SerializeField] GameObject tooltipImg;
    [SerializeField] Text tooltipText;

    TextAsset asset;
    XMLSettings UIelement;

    public void OnPointerDown(PointerEventData eventData)
    {
        circle.gameObject.SetActive(true);
        CheckLanguage();
        tooltipImg.gameObject.SetActive(true);
        StartCoroutine(Click());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        circle.gameObject.SetActive(false);
        tooltipImg.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    IEnumerator Click()
    {
        yield return new WaitForSeconds(2f);
        optionsPanel.SetActive(true);
    }

    void CheckLanguage()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Options");
        UIelement = XMLSettings.Load(asset);

        tooltipText.text = UIelement.UIelements[6].text;
    }
}
