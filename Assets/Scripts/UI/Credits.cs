﻿using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [Header("Headers")]
    [SerializeField] Text creatorsHeader;
    [SerializeField] Text sourcesHeader;

    [Header("Text")]
    [SerializeField] Text gameDevText;
    [SerializeField] Text gameDevName;

    [SerializeField] Text animatorText;
    [SerializeField] Text animatorName;

    TextAsset asset;
    XMLSettings UIelement;

    void Start()
    {
        CheckLanguage();
    }

    public void OnOpenYoutubeAudiolibrary()
    {
        Application.OpenURL("https://www.youtube.com/audiolibrary");
    }
    public void OnOpenFreesound()
    {
        Application.OpenURL("https://freesound.org/");
    }
    public void OnOpenPixabay()
    {
        Application.OpenURL("https://pixabay.com");
    } 
    
    public void OnOpenFreepik()
    {
        Application.OpenURL("https://freepik.com");
    }

    public void CheckLanguage()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Credits");
        UIelement = XMLSettings.Load(asset);

        creatorsHeader.text = UIelement.UIelements[0].text;
        gameDevText.text = UIelement.UIelements[2].text;
        gameDevName.text = UIelement.UIelements[3].text;
        animatorText.text = UIelement.UIelements[4].text;
        animatorName.text = UIelement.UIelements[5].text;
        sourcesHeader.text = UIelement.UIelements[6].text;
    }

}
