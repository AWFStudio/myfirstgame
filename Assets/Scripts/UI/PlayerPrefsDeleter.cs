﻿using UnityEngine;

public class PlayerPrefsDeleter : MonoBehaviour
{
    public void Press()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        Debug.Log("Player Prefs deleted");
    }

}
