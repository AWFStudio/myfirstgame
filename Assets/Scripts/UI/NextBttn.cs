﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBttn : MonoBehaviour
{
    [SerializeField] Animation anim;
    private void OnEnable()
    {
        anim.Play("Increase Bttn");
    }
}
