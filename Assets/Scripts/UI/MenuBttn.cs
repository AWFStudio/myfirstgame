﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBttn : MonoBehaviour
{
    [SerializeField] int sceneID;
    public void Press()
    {
        SceneManager.LoadScene(sceneID);
    }
}
