﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;

public class Music : MonoBehaviour
{
    public static Music instance = null;
    [SerializeField] AudioMixerGroup mixer;
    [SerializeField] List<AudioSource> bcgroundMusic;
     AudioSource currentMusic;

    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log("One Music Destroyed");
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        CheckMusicOptions();
        PlayRandomMusic();
    }

    public void CheckMusicOptions()
    {
        if (!PlayerPrefs.HasKey("MusicEnabled"))
        {
            PlayerPrefs.SetInt("MusicEnabled", 1);
            PlayerPrefs.Save();
        }

        if (!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetFloat("MusicVolume", 1);
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.GetInt("MusicEnabled") == 1)
        {
            mixer.audioMixer.SetFloat("MusicVol", Mathf.Log10(PlayerPrefs.GetFloat("MusicVolume")) * 20);

        }
        else if (PlayerPrefs.GetInt("MusicEnabled") == 0)
        {
            mixer.audioMixer.SetFloat("MusicVol", -80);
        }
    }

    public void Pause()
    {
        currentMusic.Pause();
    }

    public void On()
    {
        currentMusic.Play();
    }

    void PlayRandomMusic()
    {
        currentMusic = bcgroundMusic[Random.Range(0, bcgroundMusic.Count)];
        currentMusic.Play();
    }
}