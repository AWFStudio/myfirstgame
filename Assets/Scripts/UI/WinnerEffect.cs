﻿using UnityEngine;

public class WinnerEffect : MonoBehaviour
{
    [SerializeField] GameObject circle;
    [SerializeField] GameObject cup;
    [SerializeField] GameObject rays;
    [SerializeField] Animation anim;
    Sounds sounds;

    private void OnEnable()
    {
        if (sounds == null)
        {
            sounds = FindObjectOfType<Sounds>();
        }
        anim.Play("Increase Image");
        sounds.littleWin.Play();
    }
    void Update()
    {
        circle.transform.Rotate(0, 0, -0.3f);
        rays.transform.Rotate(0, 0, 0.3f);
    }
}
