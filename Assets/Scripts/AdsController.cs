﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class AdsController : MonoBehaviour, IUnityAdsListener
{
    string gameId = "3638181";
    bool testMode = true; //тест мод включен
    Button bttn;
    Music music;
    RotationsController rotations;

    void Awake()
    {
        bttn = gameObject.GetComponent<Button>();
        music = FindObjectOfType<Music>();
        rotations = FindObjectOfType<RotationsController>();
    }

    private void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(gameId, testMode);
            bttn.interactable = Advertisement.IsReady("rewardedVideo");
            if (bttn) bttn.onClick.AddListener(ShowRewardedAds);
            Advertisement.AddListener(this);
        }
    }
    public void ShowRewardedAds()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        bttn.interactable = true;
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.Log(" Unity Ads Error!");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        music.Pause();
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        music.On();
        if (showResult == ShowResult.Finished)
        {
            bttn.interactable = false;
            ChooseReward();
        }
        else if (showResult == ShowResult.Skipped)
        {
            //do not reward Player
        }
        else if (showResult == ShowResult.Failed)
        {
            //do not reward Player
        }
    }

    void OnDisable()
    {
        Advertisement.RemoveListener(this);
    }

    void ChooseReward()
    {
        if (rotations != null)
        {
            rotations.RewardPlayerForAds();
        }
    }

}
