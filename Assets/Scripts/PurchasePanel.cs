﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PurchasePanel : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI headerText;
    [SerializeField] TextMeshProUGUI adsBttnText;
    [SerializeField] TextMeshProUGUI cancelText;

    TextAsset asset;
    XMLSettings UIelement;

    void Start()
    {
        CheckLanguage();
    }
    void CheckLanguage()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Options");
        UIelement = XMLSettings.Load(asset);
        headerText.text = UIelement.UIelements[5].text;
        adsBttnText.text = UIelement.UIelements[7].text;
        cancelText.text = UIelement.UIelements[8].text;
    }
}
