﻿using System.Collections;
using UnityEngine;

public class SortingImageCatcher : MonoBehaviour
{
    [SerializeField] int zonesAmount;
    [SerializeField] int zoneNum;
    SortingController image;
    CanvasGroup canvasGroup;
    bool isActive = true;
    Sounds sounds;

    private void Awake()
    {
        sounds = FindObjectOfType<Sounds>();
        image = FindObjectOfType<SortingController>();
        if (GetComponent<CanvasGroup>() != null)
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<SortingItemMover>() != null)
        {
            if (zonesAmount == 1)
            {
                if (other.GetComponent<SortingItemMover>().ZoneNum == zoneNum)
                {
                    other.GetComponent<SortingItemMover>().TargetPosition = GetComponent<RectTransform>().position;
                    other.GetComponent<SortingItemMover>().IsCatched = true;
                    if (canvasGroup != null)
                    {
                        canvasGroup.alpha = 0;
                    }
                    sounds.correct.Play();
                    StartCoroutine(AddCount());
                }
            }
            else if (zonesAmount == 2)
            {
                if (isActive)
                {
                    if (other.GetComponent<SortingItemMover>().ZoneNum1 == zoneNum || other.GetComponent<SortingItemMover>().ZoneNum2 == zoneNum)
                    {
                        other.GetComponent<SortingItemMover>().TargetPosition = GetComponent<RectTransform>().position;
                        other.GetComponent<SortingItemMover>().IsCatched = true;
                        if (canvasGroup != null)
                        {
                            canvasGroup.alpha = 0;
                        }
                        sounds.correct.Play();
                        StartCoroutine(AddCount());
                        isActive = false;
                    }
                }

            }
        }
    }

    IEnumerator AddCount()
    {
        yield return new WaitForSeconds(0.5f);
        image.DetailsCounter--;
        image.CheckCount();
    }
}
