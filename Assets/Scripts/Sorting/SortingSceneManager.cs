﻿using System.Collections.Generic;
using UnityEngine;

public class SortingSceneManager : MonoBehaviour
{
    public GameObject nextBttn;
    public GameObject winnerImage;

    int levelNum;
    SortingController currentLevel;
    Canvas canvas;

    private void Awake()
    {
        canvas = FindObjectOfType<Canvas>();
    }

    void Start()
    {
        if (!PlayerPrefs.HasKey("SortingLevelNum"))
        {
            PlayerPrefs.SetInt("SortingLevelNum", 1);
            PlayerPrefs.Save();
        }
        levelNum = PlayerPrefs.GetInt("SortingLevelNum");
        ShowNewLevel();
    }

    void ShowNewLevel()
    {
        GameObject newLevel = Instantiate(Resources.Load<GameObject>("Prefabs/Sorting/Sorting " + levelNum));
        newLevel.transform.SetParent(canvas.transform, false);
        newLevel.transform.SetAsFirstSibling();
        currentLevel = FindObjectOfType<SortingController>();
    }

    public void ContinueBttn()
    {
        if (levelNum < 10)
        {
            levelNum++;
        }
        else
        {
            levelNum = 1;
        }
        Destroy(currentLevel.gameObject);
        winnerImage.SetActive(false);
        PlayerPrefs.SetInt("SortingLevelNum", levelNum);
        PlayerPrefs.Save();
        ShowNewLevel();
        nextBttn.SetActive(false);
        if (!PlayerPrefs.HasKey("IsTraining"))
        {
            PlayerPrefs.SetInt("IsTraining", 0);
        }
    }
}
