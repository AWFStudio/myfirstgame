﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SortingItemMover : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{

    [SerializeField] int zoneNum;
    [SerializeField] int zoneNum1;
    [SerializeField] int zoneNum2;

    float speed = 600f;

    bool isCatched;
    bool returnToStartingPosition = false;

    Vector3 startingPosition;
    Vector3 targetPosition;

    RectTransform rectTransform;
    Canvas canvas;
    CanvasGroup canvasGroup;
    GameObject myParent;
    Sounds sounds;

    public int ZoneNum { get => zoneNum; set => zoneNum = value; }
    public bool IsCatched { get => isCatched; set => isCatched = value; }
    public Vector3 TargetPosition { get => targetPosition; set => targetPosition = value; }
    public int ZoneNum1 { get => zoneNum1; set => zoneNum1 = value; }
    public int ZoneNum2 { get => zoneNum2; set => zoneNum2 = value; }

    void Update()
    {
        if (returnToStartingPosition == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, startingPosition, speed * Time.deltaTime);
        }
        if (isCatched == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }
    }

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = FindObjectOfType<Canvas>();
        sounds = FindObjectOfType<Sounds>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startingPosition = rectTransform.position;
        returnToStartingPosition = false;
        myParent = transform.parent.gameObject;
        transform.SetParent(canvas.transform);
        transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isCatched == false)
        {
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (IsCatched == false)
        {
            returnToStartingPosition = true;
            sounds.wrong.Play();
        }
        transform.SetParent(myParent.transform);

    }

}
