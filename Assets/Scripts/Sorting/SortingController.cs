﻿using System.Collections;
using UnityEngine;

public class SortingController : MonoBehaviour
{
    [SerializeField] int detailsCounter;
    [SerializeField] bool isTraining = false;
    [SerializeField] GameObject finger;
    Sounds sounds;
    SortingSceneManager scene;

    public int DetailsCounter { get => detailsCounter; set => detailsCounter = value; }


    void Awake()
    {
        sounds = FindObjectOfType<Sounds>();
        scene = FindObjectOfType<SortingSceneManager>();
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("IsTraining"))
        {
            if(PlayerPrefs.GetInt("IsTraining") == 0)
            {
                isTraining = false;
            }
            else if (PlayerPrefs.GetInt("IsTraining") == 1)
            {
                isTraining = true;
            }
        }
        if (isTraining)
        {
        StartCoroutine(HowToPlay());
        }
    }

    public void CheckCount()
    {
        if (DetailsCounter == 0)
        {
            StartCoroutine(Win());
        }
    }

    IEnumerator Win()
    {
        scene.winnerImage.SetActive(true);
        sounds.littleWin.Play();
        yield return new WaitForSeconds(1.5f);
        scene.nextBttn.gameObject.SetActive(true);
    }

    IEnumerator HowToPlay()
    {
        yield return new WaitForSeconds(0.5f);
        finger.SetActive(true);
        finger.GetComponent<Animation>().Play("Hand 1");
        float clipLength = finger.GetComponent<Animation>().GetClip("Hand 1").length;
        yield return new WaitForSeconds(clipLength);
        finger.SetActive(false);
    }


}
