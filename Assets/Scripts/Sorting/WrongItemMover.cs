﻿using UnityEngine;
using UnityEngine.EventSystems;

public class WrongItemMover : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{

    GameObject sortingPanel;
    Canvas canvas;
    RectTransform rectTransform;
    Vector3 startingPosition;
    Sounds sounds;

    bool returnToStartingPosition = false;

    float speed = 600f;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvas = FindObjectOfType<Canvas>();
        sortingPanel = FindObjectOfType<SortingController>().gameObject;
        sounds = FindObjectOfType<Sounds>();
    }

    void Update()
    {
        if (returnToStartingPosition == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, startingPosition, speed * Time.deltaTime);
            transform.SetParent(sortingPanel.transform, true);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startingPosition = rectTransform.position;
        transform.SetParent(canvas.transform, true);
        transform.SetAsLastSibling();
        returnToStartingPosition = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
            returnToStartingPosition = true;
            sounds.wrong.Play();
    }

}
