﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    [SerializeField] Animation anim;
    [SerializeField] WhoIsDifferent controller;
    Sounds sounds;
    private void OnEnable()
    {
        if (sounds == null)
        {
            sounds = FindObjectOfType<Sounds>();
        }
        StartCoroutine(Draw());
    }

    IEnumerator Draw()
    {
        anim.Play("circle");
        sounds.pencil.Play();
        while (anim.isPlaying)
        {
            yield return null;
        }
        controller.ShowWinnerEffect();
    }

}
