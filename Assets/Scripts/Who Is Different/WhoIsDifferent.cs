﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhoIsDifferent : MonoBehaviour
{
    [SerializeField] List<Sprite> sprites;
    [SerializeField] List<Image> wrongImages;
    [SerializeField] Transform correctImageTransform;
    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] GameObject winnerEffect;
    [SerializeField] GameObject nextBttn;
    [SerializeField] Transform imagesPanel;

    Image correctImage;
    Sounds sounds;
    List<Sprite> tempList = new List<Sprite>();

    void Start()
    {
        SetValues();
        GenerateLevel();
    }
    void SetValues()
    {
        sounds = FindObjectOfType<Sounds>();
        correctImage = correctImageTransform.GetComponent<Image>();
    }

    private void GenerateLevel()
    {
        tempList.AddRange(sprites);
        SelectWrongImage();
        SelectCorrectImage();
        SelectCorrectPlace();
        StartCoroutine(ShowLevel());
    }

    void SelectCorrectPlace()
    {
        int correctPlace = Random.Range(0, 6);
        correctImageTransform.SetSiblingIndex(correctPlace);
    }
    void SelectCorrectImage()
    {
        int randomSprite = Random.Range(0, tempList.Count);
        correctImage.sprite = tempList[randomSprite];
        tempList.Remove(tempList[randomSprite]);
    }
    void SelectWrongImage()
    {
        int randomSprite = Random.Range(0, tempList.Count);
        for (int i = 0; i < wrongImages.Count; i++)
        {
            wrongImages[i].sprite = tempList[randomSprite];
        }
        tempList.Remove(tempList[randomSprite]);
    }

    IEnumerator ShowLevel()
    {
        for (int i = 0; i < imagesPanel.childCount; i++)
        {
            yield return new WaitForSeconds(1f);
            imagesPanel.GetChild(i).gameObject.SetActive(true);
            sounds.turn.Play();
        }
        canvasGroup.blocksRaycasts = true;
    }

    public void BlockRaycasts()
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void ShowWinnerEffect()
    {
        winnerEffect.SetActive(true);
        nextBttn.SetActive(true);
    }

    public void RestartLevel()
    {
        for (int i = 0; i < imagesPanel.childCount; i++)
        {
            imagesPanel.GetChild(i).gameObject.SetActive(false);
        }
        winnerEffect.SetActive(false);
        nextBttn.SetActive(false);
        GenerateLevel();
    }
}
