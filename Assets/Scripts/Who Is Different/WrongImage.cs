﻿using UnityEngine;
using UnityEngine.EventSystems;

public class WrongImage : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] Animation anim;
    Sounds sounds;

    void Start()
    {
        sounds = FindObjectOfType<Sounds>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        anim.Play("Enlarge and return");
        sounds.wrong.Play();
    }
}
