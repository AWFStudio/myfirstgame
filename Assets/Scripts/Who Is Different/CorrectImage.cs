﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class CorrectImage : MonoBehaviour, IPointerClickHandler
{
    float countdown;
    [SerializeField] Animation anim;
    [SerializeField] WhoIsDifferent controller;
    [SerializeField] GameObject circle;
    Sounds sounds;

    void OnEnable()
    {
        if (sounds == null)
        {
            sounds = FindObjectOfType<Sounds>();
        }
        countdown = 10f;
    }

    void Update()
    {
        if (countdown <= 0)
        {
            anim.Play("Increase Bttn");
            countdown = 10f;
        }
        else
        {
            countdown -= Time.deltaTime;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        controller.BlockRaycasts();
        circle.SetActive(true);
    }

    void OnDisable()
    {
        circle.SetActive(false);
    }
}
