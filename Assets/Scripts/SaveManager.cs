﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance = null;

    string data;
    string path;

    public int allRotations = 2;
    public int acquiredRotations;

    private void Start()
    {
        CheckInstance();
        CheckData();
        Load();
        CheckForSaves();
    }

    public void Save()
    {
        data = JsonUtility.ToJson(this);
        File.WriteAllText(path, data);
    }

    void Load()
    {
        data = File.ReadAllText(path);
        JsonUtility.FromJsonOverwrite(data, this);
    }

    void CheckInstance()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log("One SaveManager Destroyed");
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void CheckData()
    {
        if (path == null)
        {
            path = Application.persistentDataPath + "/diskrunnerData.txt";
        }
        if (!File.Exists(path))
        {
            File.Create(path);
        }
    }

    void CheckForSaves()
    {
        if (acquiredRotations == 0)
        {
            acquiredRotations = 1;
        }
    }
}
