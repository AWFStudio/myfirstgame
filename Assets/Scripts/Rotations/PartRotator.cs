﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartRotator : MonoBehaviour
{
    [SerializeField] Animation anim;
    [SerializeField] int pos;
    RotationsController controller;
    bool isEnable = true;
    Sounds sounds;

    public int Pos { get => pos; }

    private void Awake()
    {
        controller = FindObjectOfType<RotationsController>();
        sounds = FindObjectOfType<Sounds>();
    }
    private void OnMouseDown()
    {
        if (isEnable)
        {
            if (pos != 0)
            {
                StartCoroutine(Turn());
            }
        }
    }

    IEnumerator Turn()
    {
        isEnable = false;
        switch (pos)
        {
            case 1:
                anim.Play("Turn 1");
                pos++;
                break;
            case 2:
                anim.Play("Turn 2");
                pos++;
                break;
            case 3:
                anim.Play("Turn 3");
                pos = 0;
                break;
        }
        sounds.turn.Play();
        while (anim.isPlaying)
        {
            yield return null;
        }
        if (pos == 0)
        {
            controller.CheckProgress();
        }
        else
        {
            isEnable = true;
        }
    }



}
