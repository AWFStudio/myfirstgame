﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationsController : MonoBehaviour
{
    [SerializeField] GameObject nextBttn;
    [SerializeField] List<Transform> rotations;
    [SerializeField] GameObject purchasePanel;
    List<PartRotator> parts = new List<PartRotator>();
    List<Transform> tempList = new List<Transform>();
    Transform currentPuzzle;
    Animation anim;
    Sounds sounds;
    SaveManager data;
    PurchasePanel purchase;
    int count = 1;

    void Start()
    {
        sounds = FindObjectOfType<Sounds>();
        data = FindObjectOfType<SaveManager>();
        purchase = purchasePanel.GetComponent<PurchasePanel>();

        ShowRandomPuzzle();
    }

    public void CheckProgress()
    {
        bool isWin = true;
        for (int i = 0; i < parts.Count; i++)
        {
            if (parts[i].Pos != 0)
            {
                isWin = false;
                break;
            }
        }
        if (isWin)
        {
            ShowWinnerEffect();
        }
    }

    public void ShowWinnerEffect()
    {
        anim.Play("RotationWinnerEffect");
        sounds.littleWin.Play();
        nextBttn.SetActive(true);
    }

    public void ShowNextPuzzle()
    {
        Destroy(currentPuzzle.gameObject);
        ShowRandomPuzzle();
    }

    void ShowRandomPuzzle()
    {
        count--;
        if (count <= 0)
        {
            purchasePanel.SetActive(true);
            count = 1;
            return;
        }

        if (tempList.Count <= 0)
        {
            for (int i = 0; i < data.acquiredRotations; i++)
            {
                tempList.Add(rotations[i]);
            }
        }

        int randomPuzzle = Random.Range(0, tempList.Count);
        currentPuzzle = Instantiate(tempList[randomPuzzle], new Vector3(0, 0, 0), Quaternion.identity);
        tempList.Remove(tempList[randomPuzzle]);
        anim = currentPuzzle.GetComponent<Animation>();
        for (int i = 0; i < currentPuzzle.childCount; i++)
        {
            parts.Add(currentPuzzle.GetChild(i).GetComponent<PartRotator>());
        }
    }

    public void ClosePurchasePanel()
    {
        purchasePanel.SetActive(false);
        ShowRandomPuzzle();
    }

    public void RewardPlayerForAds()
    {
        data.acquiredRotations++;
        data.Save();
        purchasePanel.SetActive(false);
        ShowPurchasedPuzzle();
    }

    void ShowPurchasedPuzzle()
    {
        currentPuzzle = Instantiate(rotations[data.acquiredRotations], new Vector3(0, 0, 0), Quaternion.identity);
        anim = currentPuzzle.GetComponent<Animation>();
        for (int i = 0; i < currentPuzzle.childCount; i++)
        {
            parts.Add(currentPuzzle.GetChild(i).GetComponent<PartRotator>());
        }
    }

}
