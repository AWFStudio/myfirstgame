﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusicBttn : MonoBehaviour
{
    [SerializeField] int musicID;
    LullabyController lullabyController;

    void Start()
    {
        lullabyController = FindObjectOfType<LullabyController>();
    }

    public void Press()
    {
        lullabyController.ChangeMusic(musicID);
    }
}
