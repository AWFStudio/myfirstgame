﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] List<Sprite> starSprites;
    [SerializeField] SpriteRenderer myRenderer;
    [SerializeField] Transform myTransform;
    [SerializeField] Animation myAnimation;

    private void OnEnable()
    {
        myRenderer.sprite = starSprites[Random.Range(0, starSprites.Count)];
        myTransform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        StartCoroutine(Twinkle());
    }

    IEnumerator Twinkle()
    {
        myAnimation.Play("Star");
        while (myAnimation.isPlaying)
        {
            yield return null;
        }
        gameObject.SetActive(false);
    }

}
