﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;


public class LullabyController : MonoBehaviour
{
    [SerializeField] List<GameObject> clouds;
    [SerializeField] List<GameObject> stars;
    [SerializeField] List<AudioSource> lullabies;

    AudioSource currentLullabies;

    void Start()
    {
        PlayRandomMusic();
        StartCoroutine(StarsOn());
        StartCoroutine(CloudsOn());
    }
    IEnumerator StarsOn()
    {
        while (true)
        {
            ShowStar();
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator CloudsOn()
    {
        while (true)
        {
            ShowCloud();
            yield return new WaitForSeconds(20f);
        }
    }

    void ShowCloud()
    {
        bool isFound = false;
        while (!isFound)
        {
            int randomCloud = Random.Range(0, clouds.Count);
            if (!clouds[randomCloud].activeSelf)
            {
                clouds[randomCloud].SetActive(true);
                isFound = true;
            }
        }
    }

    void ShowStar()
    {
        bool isFound = false;
        while (!isFound)
        {
            int randomStar = Random.Range(0, stars.Count);
            if (!stars[randomStar].activeSelf)
            {
                stars[randomStar].SetActive(true);
                isFound = true;
            }
        }
    }

    void PlayRandomMusic()
    {
        currentLullabies = lullabies[Random.Range(0, lullabies.Count)];
        currentLullabies.Play();
    }

    public void ChangeMusic(int musicID)
    {
        currentLullabies.Stop();
        currentLullabies = lullabies[musicID];
        currentLullabies.Play();
    }
}


