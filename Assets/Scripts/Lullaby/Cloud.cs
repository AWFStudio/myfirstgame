﻿using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    float speed = 0.5f;
    Transform myTransform;
    [SerializeField] List<Sprite> cloudSprites;
    [SerializeField] SpriteRenderer myRenderer;

    private void OnEnable()
    {
        if (myTransform == null)
        {
            myTransform = GetComponent<Transform>();
        }
        myTransform.position = new Vector3(-15f, Random.Range(-6f, 3f), 0);
        myRenderer.sprite = cloudSprites[Random.Range(0, cloudSprites.Count)];
    }
    void Update()
    {
        if (myTransform.position.x > 30f)
        {
            gameObject.SetActive(false);
        }
        else
        {
            myTransform.position = new Vector3(myTransform.position.x + Time.deltaTime * speed, myTransform.position.y, 0);
        }
    }
}
