﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene : MonoBehaviour
{
    [SerializeField] protected GameObject winnerEffect;
    [SerializeField] protected GameObject nextLevelBttn;
    [SerializeField] protected List<Transform> levels;
    [SerializeField] protected Transform myCanvas;

    List<Transform> tempLevelsList;
    Transform currentLevel;

    public virtual void Start()
    {
        ShowRandomLevel();
    }

    void ShowRandomLevel()
    {
        if (tempLevelsList.Count <= 0)
        {
            tempLevelsList.AddRange(levels);
        }
        int randomLevel = Random.Range(0, tempLevelsList.Count);
        currentLevel = Instantiate(tempLevelsList[randomLevel]);
        currentLevel.SetParent(myCanvas, false);
        currentLevel.SetAsFirstSibling();
    }

    public void NextLevelBttn()
    {
        winnerEffect.SetActive(false);
        Destroy(currentLevel);
        ShowRandomLevel();
        nextLevelBttn.SetActive(false);
    }

    void ShowWinEffect()
    {
        winnerEffect.SetActive(true);
        nextLevelBttn.SetActive(true);
    }
}




